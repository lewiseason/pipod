require 'json'
require 'ostruct'

class Schedule

	def initialize(data)
		@data = OpenStruct.new JSON.parse(data)

		@playlists = []
		@data.playlists.each do |playlist|
			@playlists.push Playlist.new(playlist)
		end
	end

	def current
		now = Time.now + 1
		candidates = []

		@playlists.each do |playlist|
			if playlist.is_valid
				candidates.push playlist
			end
		end

		# Sort by precidence
		candidates.sort_by! { |x| x.precidence }

		# Return the first one
		candidates[0]
	end

end