CurrentPlaylistDeleter = lambda do
	Support::debug 'Starting CurrentPlaylistDeleter'
	begin
		loop do

			Support::debug 'Wait for a while (See Appdata.current_playlist_delete_interval)'
			sleep Appdata.current_playlist_delete_interval
		end
	rescue
		Support::error $!
	end
end