module API
	include HTTParty
	base_uri "#{Appdata.uri_stub}"
	format :json

	def self.get_schedule(zone)
		attempts = 0
		success = false
		max_attempts = Appdata.max_network_attempts

		while attempts < max_attempts do
			Support::debug 'Attempting to fetch schedule'
			begin
				schedule = get( '/schedule', :query => { :zone => zone } )
				success = true
				break
			rescue Exception => e
				attempts += 1
			end
		end

		if success
			Support::debug 'Successfully fetched schedule'
			return schedule
		else
			Support::debug 'Failed to fetch schedule'
			return false
		end
	end

	def self.skipped(item)
		# TODO: Implement
	end
end

module RemoteDatastore
	include HTTParty
	base_uri "#{Appdata.remote_datastore}"

	def self.get_item(item, destination)

		attempts = 0
		until attempts > 5 do
			attempts += 1

			response = self.download item.location

			if response
				if response.code == 200
					if self.write_file(response, destination)
						return true
					end
				else
					next
				end
			else
				next
			end
		end

		return nil
	end

	private

	def self.download(remote)
		begin
			response = get remote
		rescue
			Support::debug 'Exception occurred when downloading'
			return nil
		end

		response
	end

	def self.write_file(response, local)
		begin
			File.open(local, 'w') do |file|
				file << response.body
			end
		rescue
			Support::debug 'An exception occurred when writing to the file'
			return nil
		end

		true
	end

end