# TODO: Yeah, this really wants to be multiple files
module Appdata
	# Make this a singleton
	# http://speakmy.name/2011/05/29/simple-configuration-for-ruby-apps
	extend self

	def parameter(*names)
		names.each do |name|
			attr_accessor name

			# For each symbol, we generate an accessor method.
			define_method name do |*values|
				value = values.first
				value ? self.send("#{name}=", value) : instance_variable_get("@#{name}")
			end
		end
	end

	def config(&block)
		instance_eval &block
	end
end

Appdata.config do
	parameter :superdebug
	parameter :nodebug
	parameter :zone
	parameter :uri_stub
	parameter :max_network_attempts

	parameter :local_datastore
	parameter :remote_datastore

	parameter :schedule_fetch_interval
	
	parameter :current_playlist_download_interval
	parameter :current_playlist_delete_interval

	parameter :current_playlist_high_watermark
	parameter :current_playlist_low_watermark
end

module Support
	require 'find'

	def debug(s)
		if !!Appdata.nodebug
			return
		end

		# Crudely display the filename and line number
		# if we've set superdebug
		if !!Appdata.superdebug
			print caller[0][/([a-z0-9_]+)\.([a-z]+)\:([0-9]+)/] + "\t"
		end

		puts s
	end

	def error(s)
		# Flag up the error, then debug it, if superdebug is set
		if !!Appdata.superdebug
			puts " ******** ERROR ******** "
			puts $!.message
			puts $!.backtrace
		else
			debug(s)
		end

	end

	def dirsize(dir)
		dirsize = 0
		Find.find(dir) do |f| 
			dirsize += File.stat(f).size
		end
		dirsize
	end
	
end

class Fixnum
	def nanoseconds
		self / 1e9
	end

	def microseconds
		self / 1e6
	end

	def milliseconds
		self / 1000
	end

	def seconds
		self
	end

	def minutes
		self * 60
	end

	def hours
		self * 3600
	end

	def days
		self * 86400
	end

	def weeks
		self * 604800
	end

	def months
		self * 2.63e6
	end

	def years
		self * 3.156e7
	end

	def bits
		self / 8
	end

	def kilobits
		self * 128
	end

	def kilobytes
		self * 1024
	end

	def megabits
		self * 131072
	end

	def megabytes
		self * 1.049e6
	end

	def gigabits
		self * 1.342e8
	end

	def gigabytes
		self * 1.074e9
	end
end