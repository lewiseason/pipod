require 'ostruct'

class Item

	def initialize(data)
		@data = OpenStruct.new data
	end

	private

	def method_missing(method, *args, &block)
		if @data.respond_to? method
			@data.send(method, *args, &block)
		else
			super
		end
	end

end