require 'ostruct'

class Playlist
	attr_reader :items

	def initialize(data)
		@data = OpenStruct.new data
		@items = []
		@data.items.each do |item|
			@items.push Item.new(item)
		end
	end

	def valid_from
		parse_time @data.valid_from
	end

	def valid_until
		parse_time @data.valid_until
	end

	def precidence
		@data.precidence
	end

	def name
		@data.name
	end

	def path
		"#{Appdata.local_datastore}/#{@data.name}"
	end

	def item(index)
		@items[index]
	end

	def is_valid
		now = Time.now
		(valid_from < now) and (now < valid_until)
	end

	def id_to_item(id=nil)
		@items.each do |item, index|
			if item.id == id
				return [item, index]
			end
		end

		return nil
	end

	def next_item(item)
		next_item_id = (item_to_index item) + 1

		# If the id is to much, reduce it until in range
		while next_item_id >= @items.count
			next_item_id -= @items.count
		end

		@items[next_item_id]
	end

	def item_to_index(item)
		@items.index(unambiguous_item(item))
	end

	private

	def parse_time(time)
		Time.parse time
	end

	def unambiguous_item(item)
		# When the schedule gets updated, the playlists all change
		# So you can't do an @items.index, since all the object_ids would be wrong
		# Instead, lookup by the ID we set ourselves on each Item
		@items.each do |real_item|
			if item.id == real_item.id
				return real_item
			end
		end
	end


end