require 'thread'
require 'httparty'
require './libs/support'
require './config'
require './libs/network'

Dir["./models/*"].each {|file| require file }


include Support

# This lock should be held when doing any operations on the
# playlist, including modifying it, or queueing a track.
$playlist_lock = Mutex.new

# This stores the most up to date copy of the schedule
# as retrieved from the server
$schedule = nil

# This will store the unique id of the item that is currently playing
# TODO: This will probably get set in the playlist enqueuer
$current_item_id = nil

# Do some sanity tests
# (check watermarks are the right way around)

# Once we've set up our environment, include all the required threads.
Dir["./threads/*"].each {|file| require file }

# We start the threads manually, rather than wrapping them
# in thread blocks in the file, since we might care about
# the order - and Thread.new's are nicer than requires, imo

# Start the schedule fetcher thread
schedule_fetcher = Thread.new(&ScheduleFetcher)

# Start the current playlist downloader thread
current_playlist_downloader = Thread.new(&CurrentPlaylistDownloader)

# Start the current playlist deleter thread
current_playlist_deleter = Thread.new(&CurrentPlaylistDeleter)

# Wait for schedule_fetcher to finish before terminating
# (yes, I know...)
begin
	schedule_fetcher.join
rescue SystemExit, Interrupt
	puts 'Caught Interrupt: Shutting Down'
	# TODO: Possibly notify the API endpoint of this?
end