require 'sinatra'
require 'json'
require 'time'

current_playlist_size = 27
next_playlist_size = 71

tracks_for_current_playlist = Array.new(current_playlist_size){ |index| {
	:id => index,
	:title => "This is the title of track #{index}",
	:length => Random.rand(20..600),
	:location => "/current.mp3",
	:name => "#{index}.mp3",
} }

tracks_for_next_playlist = Array.new(next_playlist_size){ |index| {
	:id => current_playlist_size + index,
	:title => "This is the title of track #{current_playlist_size + index}",
	:length => Random.rand(20..600),
	:location => "/next.mp3",
	:name => "#{index}.mp3",
} }

get '/schedule' do
	content_type :json
	content = {
		:zone => 'library',
		:generated => Time.now,
		:playlists => [
			{
				:id => 113,
				:valid_from => (Time.now - 3600).iso8601,
				:valid_until => (Time.now + (3600*30)).iso8601,
				:name => 'Current',
				:items => tracks_for_current_playlist,
				:precidence => 1,
			},
			{
				:id => 228,
				:valid_from => (Time.now + (3600*30)).iso8601,
				:valid_until => (Time.now + (3600*120)).iso8601,
				:name => 'Next',
				:items => tracks_for_next_playlist,
				:precidence => 1,
			},
		]
	}
	JSON.pretty_generate(content)
end
