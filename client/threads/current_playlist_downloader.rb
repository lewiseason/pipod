CurrentPlaylistDownloader = lambda do
	last_download = nil

	Support::debug 'Starting CurrentPlaylistDownloader'
	begin
		loop do
			if $schedule
				Support::debug 'Wait for playlist lock'
				$playlist_lock.synchronize {
					current = $schedule.current
					cdir = current.path

					if !File.directory? cdir
						Dir.mkdir(cdir, 0750)
					end
					
					if Support::dirsize($schedule.current.path) <= Appdata.current_playlist_high_watermark
						Support::debug 'We have enough space'

						iterations = 0
						until iterations > current.items.count do
							iterations += 1
							
							# Find the song after the one we last tried to download
							if last_download
								next_item = current.next_item(last_download)
							else
								next_item = current.items[0]
							end

							Support::debug "The next item to fetch is #{next_item.title}"
							last_download = next_item

							file_name = File.join(current.path, next_item.name)
							if File.exist? file_name
								Support::debug 'File already downloaded'
								next
							end

							Support::debug 'Attempting download'
							result = RemoteDatastore::get_item(next_item, file_name)

							# If get_item returned true, a file was downloaded
							break if result == true
						end
					else
						Support::debug 'We do not have enough space'
					end
				}
			end

			Support::debug 'Wait for a while (See Appdata.current_playlist_download_interval)'
			sleep Appdata.current_playlist_download_interval
		end
	rescue
		Support::error $!
	end
end