ScheduleFetcher = lambda do
	Support::debug 'Starting ScheduleFetcher'
	begin
		loop do
			Support::debug 'Query Server for Schedule'
			schedule = API::get_schedule(Appdata.zone)

			# TODO: If NOT MODIFIED, don't create a new schedule object, or get
			# the lock or anything. That's stupid

			# If we managed to download it
			if schedule

				Support::debug 'Wait for playlist lock'
				$playlist_lock.synchronize {

					$schedule = Schedule.new(schedule.body)
					Support::debug "Current playlist is: #{$schedule.current.name}"
					
				}

				Support::debug 'Released playlist lock'

			end

			Support::debug 'Wait for a while (See Appdata.schedule_fetch_interval)'
			sleep Appdata.schedule_fetch_interval
		end
	rescue
		Support::error $!
	end
end
